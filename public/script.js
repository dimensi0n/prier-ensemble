const scrollCaroussel = () => {
    const caroussel = document.getElementById('caroussel')
    const toScroll = Array.from(document.getElementsByClassName('citation'))[0].offsetWidth
    if (caroussel.scrollLeft > toScroll * (caroussel.childElementCount - 1)) {
        caroussel.scrollTo({
            left: 0,
            behavior: 'smooth'
        })
    } else {
        caroussel.scrollTo({
            left: caroussel.scrollLeft + (toScroll + (toScroll /2)),
            behavior: 'smooth'
        })
    }
}

window.onload = () => {
    document.getElementById('point').style.opacity = 100;
    Array.from(document.getElementsByClassName('division')).forEach((element) => element.style.width = '100%')
    document.getElementById('caroussel') 
    setInterval(scrollCaroussel,5000)
}
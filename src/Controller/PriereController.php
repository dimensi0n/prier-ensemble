<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;
use App\Form\PriereType;
use App\Entity\Priere;

class PriereController extends AbstractController
{
    /**
     * @Route("/priere", name="priere")
     */
    public function index(): Response
    {
        return $this->render('priere/index.html.twig', [
            'controller_name' => 'PriereController',
        ]);
    }

    /**
     * @Route("/priere/create", name="create-priere")
     */
    public function create(Request $request, FileUploader $fileUploader): Response
    {
        // just setup a fresh $task object (remove the example data)
        $priere = new Priere();

        $form = $this->createForm(PriereType::class, $priere);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$priere` variable has also been updated
            $priere = $form->getData();
            $image = $form->get('Image')->getData();
            if ($image) {
                $imageFileName = $fileUploader->upload($image);
                $priere->setImageFilename($imageFileName);
            }
            $priere->setBackground();
            $priere->setContent(str_replace('\n', '<br>', $priere->getContent()));
            $priere->setPending(false);

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($priere);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('priere/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

<?php

namespace App\Entity;

use App\Repository\PriereRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PriereRepository::class)
 */
class Priere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="text")
     */
    private $Content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ImageFilename;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ImageDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Author;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $Pending;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Background;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }

    public function getImageFilename(): ?string
    {
        return $this->ImageFilename;
    }

    public function setImageFilename(?string $ImageFilename): self
    {
        $this->ImageFilename = $ImageFilename;

        return $this;
    }

    public function getImageDescription(): ?string
    {
        return $this->ImageDescription;
    }

    public function setImageDescription(?string $ImageDescription): self
    {
        $this->ImageDescription = $ImageDescription;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->Author;
    }

    public function setAuthor(?string $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getPending(): ?bool
    {
        return $this->Pending;
    }

    public function setPending(bool $Pending): self
    {
        $this->Pending = $Pending;

        return $this;
    }

    public function getBackground(): ?string
    {
        return $this->Background;
    }

    public function setBackground(): self
    {
        $this->Background = $this->id % 2 != 0 ? 'Blue' : Null;

        return $this;
    }
}

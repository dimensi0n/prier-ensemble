<?php

namespace App\Repository;

use App\Entity\Priere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Priere|null find($id, $lockMode = null, $lockVersion = null)
 * @method Priere|null findOneBy(array $criteria, array $orderBy = null)
 * @method Priere[]    findAll()
 * @method Priere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriereRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Priere::class);
    }

    /**
    * @return Priere[] Returns an array of Priere objects
    */
    public function getValidatedPrieres()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.Pending = false')
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Priere
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Citation;

class CitationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $sagesse = new Citation();
        $sagesse->setContent('Heureux qui trouve la sagesse, qui accède à la raison !');
        $sagesse->setAuthor('Pr 3, 13');

        $insenses = new Citation();
        $insenses->setContent('Qui fait route avec les sages deviendra sage ; qui fréquente les insensés tournera mal. Le malheur poursuit les pécheurs, le bonheur récompense les justes');
        $insenses->setAuthor('Pr 13, 20-21');

        $heureux = new Citation();
        $heureux->setContent('Heureux les yeux qui voient ce que vous voyez !');
        $heureux->setAuthor('Lc 10, 23');

        $manager->persist($sagesse);
        $manager->persist($insenses);
        $manager->persist($heureux);
        $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Priere;

class PriereFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $symeon = new Priere();
        $symeon->setTitle('Cantique de Syméon');
        $symeon->setAuthor('Luc 2, 29-32');
        $symeon->setContent('“Maintenant, ô Maître souverain, tu peux laisser ton serviteur s’en aller en paix, selon ta parole. Car mes yeux ont vu le salut que tu préparais à la face des peuples : lumière qui se révèle aux nations et donne gloire à ton peuple Israël." Puis conclure en disant : “Gloire au Père, au Fils et au Saint Esprit Comme il était au commencement, maintenant et toujours, et dans les siècles des siècles Amen.”');
        $symeon->setImageFilename('symeon.jpg');
        $symeon->setImageDescription('Cantique de Syméon
        Aert de Gelder (1645–1727)
        Huile sur toile, 107,5 x 94,5 cm ');
        $symeon->setPending(false);
        $symeon->setBackground();

        $magnificat = new Priere();
        $magnificat->setTitle('Magnificat');
        $magnificat->setContent('Mon âme exalte le Seigneur, exulte mon esprit en Dieu, mon Sauveur ! Il s’est penché sur son humble servante ; désormais, tous les âges me diront bienheureuse. Le Puissant fit pour moi des merveilles ; Saint est son nom ! Son amour s’étend d’âge en âge sur ceux qui le craignent. Déployant la force de son bras, il disperse les superbes. Il renverse les puissants de leurs trônes, il élève les humbles. Il comble de biens les affamés, renvoie les riches les mains vides. Il relève Israël son serviteur, il se souvient de son amour, de la promesse faite à nos pères, en faveur d’Abraham et de sa race, à jamais. Gloire au Père, et au Fils, et au Saint-Esprit, pour les siècles des siècles. Amen.');
        $magnificat->setImageFilename('magnificat.jpg');
        $magnificat->setImageDescription('Botticelli, Vierge Madonna Fu Magnificat, Detrempe sur bois, 1483');
        $magnificat->setPending(false);
        $magnificat->setBackground();


        $bonhoeffer = new Priere();
        $bonhoeffer->setTitle('Prière du soir de Dietrich Bonhoeffer');
        $bonhoeffer->setContent(str_replace('\n', '<br>', 'Seigneur Dieu, je te rends grâce d\'avoir mené à terme cette journée:
        Je te rends grâce d’apaiser mon corps et mon âme.
        Ta main était sur moi et m’a gardé et préservé. Pardonne mon manque de foi et tout le mal de cette journée.
        Aide-moi à pardonner à ceux qui m’ont fait tort.
        Fais-moi dormir paisiblement sous ta garde et préserve-moi des tentations de la nuit.
        Je te confie les miens et cette maison, je te confie mon corps et mon âme.
        Que ton saint nom soit loué.'));
        $bonhoeffer->setImageFilename('bonhoeffer.jpg');
        $bonhoeffer->setImageDescription('Francesco de\' Rossi, La Vierge qui prie, 1640-50');
        $bonhoeffer->setBackground();
        $bonhoeffer->setPending(false);

        $trinite = new Priere();
        $trinite->setTitle('Prière d\'Elisabeth de la Trinité');
        $trinite->setContent(str_replace('\n', '<br>', 'Sois là, Seigneur, soutiens-moi.
        Détache mon cœur de tout, qu’il soit bien libre pour que rien ne l’empêche de Te voir.
        Brise ma volonté, abaisse mon orgueil, ô Toi si humble de cœur.
        Mon cœur, façonne-le pour qu’il puisse être Ta demeure aimée,
        pour que Tu viennes y reposer, y converser avec moi dans une idéale union.
        Que ce pauvre cœur, Seigneur, ne fasse plus qu’un avec le Tien.
        Toi seul peux combler sa solitude. Que je ne cherche rien en dehors de Toi seul
        Tu es capable de me contenter.
        Ainsi soit-il.'));
        $trinite->setImageFilename('trinite.jpg');
        $trinite->setImageDescription('Luca Rossetti, Trinità Chiesa');
        $trinite->setPending(false);
        $trinite->setBackground();

        $manager->persist($symeon);
        $manager->persist($magnificat);
        $manager->persist($bonhoeffer);
        $manager->persist($trinite);
        $manager->flush();
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122175816 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE priere ADD COLUMN pending BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE priere ADD COLUMN background VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__priere AS SELECT id, title, content, image_filename, image_description, author FROM priere');
        $this->addSql('DROP TABLE priere');
        $this->addSql('CREATE TABLE priere (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content CLOB NOT NULL, image_filename VARCHAR(255) DEFAULT NULL, image_description VARCHAR(255) DEFAULT NULL, author VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO priere (id, title, content, image_filename, image_description, author) SELECT id, title, content, image_filename, image_description, author FROM __temp__priere');
        $this->addSql('DROP TABLE __temp__priere');
    }
}
